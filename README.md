# HOW IT WORKS

So you  wanna create a bootable USB key using our script ?

To do so, it is really simple, just run `sudo ./script.sh`.
Next, just answer the questions it will show you.
And that's it !

Please make sure mklibs is installed as it makes sure to install all the librairies needed.

Please, take notes that the script will prompt you to configure busybox and the kernel.

In details :

- Format the key, you should answer yes to start from a clean state
- Partition, you should have two primary partitions of 200Mo, the first one with a bootable flag and the second one a FAT32 type
- wait...

And it should be working. 
