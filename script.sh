#!/bin/bash

# si root
if [[ $EUID -ne 0 ]]; then
   echo "Ce script doit être executé en tant que root !" 
   exit 1
fi

# Vérification des sources
if [ ! -f src/busybox-1.33.0.tar.bz2 ]; then
	wget "https://busybox.net/downloads/busybox-1.33.0.tar.bz2" -O src/busybox-1.33.0.tar.bz2
else
	echo "sources de bb présentes"
fi

if [ ! -f src/linux-5.11-rc5.tar.gz ]; then
	wget "https://github.com/torvalds/linux/archive/v5.11-rc5.tar.gz" -O src/linux-5.11-rc5.tar.gz
else
	echo "sources du kernel présentes"
fi

if [ ! -f src/fbv.zip ]; then
	wget "https://github.com/godspeed1989/fbv/archive/master.zip" -O src/fbv.zip
else
	echo "Sources de fbv présentes"
fi

if [ ! -f src/ncurses-6.2.tar.gz ]; then
	wget "https://ftp.gnu.org/pub/gnu/ncurses/ncurses-6.2.tar.gz" -O src/ncurses-6.2.tar.gz
fi

if [ ! -f src/SDL2-2.0.14.tar.gz ]; then
	wget "https://libsdl.org/release/SDL2-2.0.14.tar.gz" -O src/SDL2-2.0.14.tar.gz
fi

if [ ! -d build ]; then
	mkdir build
fi

if [ ! -d src ]; then
	mkdir src
fi

read -p "Clean build ? [y-N] " bld 
if [ $bld == "y" ]; then
   rm -rf build/*
fi


# Démontage de toutes les partitions montée
umount /mnt/*
umount /media/virgile/*

# Affichage des clés usb disponibles
fdisk -l

read -p "Entrez l'identifiant de la clé usb (/dev/sdX) (juste le sdX)" usb
export usb=/dev/$usb
echo "Clé usb : "$usb


read -p "Formater la clé ? [y-N] " format 
if [ $format == "y" ]; then
   # Zap de la clé usb (vidage de la table des partitions / gpt/mbr)
   sgdisk --zap-all $usb
fi

# scan des nouvelles partitions
partprobe

read -p "Partitionner ? [y-N] " part
if [ $part == "y" ]; then
   # fdisk, création des partitions
   fdisk $usb
fi
partprobe

# pause, le temps que la clé retrouve ses esprits
sleep 3

# Création des partitions

mkfs.ext4 $usb"1"
# vfat pour la partition efi
mkfs.vfat -F32 $usb"2"

# Montage des partitions

export sys=/mnt/sys
export efi=/mnt/efi

mkdir $sys
mkdir $efi

mount $usb"1" $sys
mount $usb"2" $efi

# Création du dossier boot GRUB

mkdir $sys/boot

# Installation de grub, pour efi et après x86_64

grub-install $usb --target=i386-pc --boot-directory=$sys/boot
grub-install $usb --boot-directory=$sys/boot --efi-directory=$efi --target=x86_64-efi --removable

# Copie kernel

export vmlz=/boot/vmlinuz
export inrd=/boot/initrd.img

cp $vmlz $sys/boot/vmlinuz
cp $inrd $sys/boot/initrd.img

# copie fichier cfg grub

cp data/grub.cfg $sys/boot/grub/

# création des dossiers non créés par busybox
echo "compilation busybox"

export curdir=$(pwd)

# Compilation de busybox

if [ ! -d build/busybox-1.33.0 ]; then

tar jxvf src/busybox-1.33.0.tar.bz2 -C build/
else
	echo "Le dossier de busybox existe déjà. On passe à la suite"
fi
cp data/busybox.config build/busybox-1.33.0/.config
cd build/busybox-1.33.0

make menuconfig

make -j9

make CONFIG_PREFIX=$sys install
mkdir $sys/etc
cp examples/inittab $sys/etc

cd $curdir

echo "Création /dev busybox"
mkdir $sys/dev; cd $sys/dev; /sbin/MAKEDEV generic console
mkdir -p $sys/etc/init.d $sys/proc $sys/sys $sys/root/run $sys/root/proc $sys/root/sys $sys/run $sys/lib $sys/bin

cd $curdir

cp data/rcS $sys/etc/init.d/rcS

chmod +x $sys/etc/init.d/rcS

echo "Mise en place keymap"
cd $sys
./bin/dumpkmap > etc/french.kmap
cd $curdir
# Démontage de la clé


echo "Mise en place du Kernel Custom"
if [ ! -d build/linux-5.11-rc5 ]; then
	tar -xzvf src/linux-5.11-rc5.tar.gz -C build/
else
	echo "Le dossier du kernel existe déjà. A la suite"
fi
cp data/kernel.config build/linux-5.11-rc5/.config
cd build/linux-5.11-rc5/
make menuconfig
make -j4
cp arch/x86/boot/bzImage $sys/boot/

# Création des utilisateurs 
echo "root:x:0:0:root:/root:/bin/sh">$sys/etc/passwd
echo "root:x:0:root">$sys/etc/group


cd $curdir

#show res
gcc src/testframeBuffer.c -o $sys/bin/show_res

# Mise en place de fbv
if [ ! -d build/fbv-master ]; then
	unzip src/fbv.zip -d build/
else
	echo "Le dossier de source de fbv existe déjà"
fi

cd build/fbv-master
./configure
make
cp fbv $sys/bin/


mklibs -v -d $sys/lib $sys/bin/*
#umount /mnt/*
cd $curdir

mkdir -p $sys/usr/share/udhcpc
cp build/busybox-1.33.0/examples/udhcp/simple.script $sys/usr/share/udhcpc/default.script
chmod +x $sys/usr/share/udhcpc/default.script

cp data/inittab $sys/etc/inittab
echo "export TERMINFO=/share/terminfo" > $sys/etc/profile


cd $curdir
# install ncurses
if [ ! -d build/ncurses-6.2 ]; then
	tar -xzvf src/ncurses-6.2.tar.gz -C build/
fi
cd build/ncurses-6.2
./configure --prefix=$sys
make -j9
make install

cd $curdir


cp src/nc build/nc
cd build/nc
make
cp shuffle $sys/bin

cd $curdir


if [ ! -d build/SDL2-2.0.14 ]; then
	tar -xzvf src/SDL2-2.0.14.tar.gz -C build/
fi
cd build/SDL2-2.0.14
./configure --prefix=$sys
make -j5
make install

cd $curdir

