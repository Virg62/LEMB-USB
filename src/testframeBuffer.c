#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

enum {BLEU, VERT, ROUGE, ALPHA};

#ifdef _DEBUG_

#define	CHECK(status, msg)						\
	{								\
		fprintf(stderr, "Status = %ld\t", (long)status);	\
		if ( -1 == (status))					\
		{							\
			perror(msg);					\
			exit(EXIT_FAILURE);				\
		} 							\
		else 							\
			fprintf(stderr, "%s ... Ok\n", msg);		\
	}		

#else

#define	CHECK(status, msg)						\
		if ( -1 == (status))					\
		{							\
			perror(msg);					\
			exit(EXIT_FAILURE);				\
		} 							
#endif


int main(void)
{
    int fbfd = 0;
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    long int screensize = 0;
    char *fbp = 0;
    int x = 0, y = 0;
    long int location = 0;

    /* Ouverture du périphérique en lecture et écriture	*/
    fbfd = open("/dev/fb0", O_RDWR);
    CHECK(fbfd, "Accès au framebuffer open()");
 

    /* Récupération d'informations d'écran 		*/
    CHECK (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo), "Lecture d'information d'écran par ioctl()");

    /* Récupépration des caractéristiques de l'écran	*/
    CHECK(ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo), "Lecture des caractéristiques de l'écran par ioctl()");

    printf("Resolution = %d x %d  avec %d bits par pixel\n", vinfo.xres, vinfo.yres, vinfo.bits_per_pixel );

    /* Calcul de la taille mémoire nécessaire au framebuffer  (en octets) */
    screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;

    /* Projeter le framebuffer en mémoire	*/
    fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);
    CHECK ((long)fbp, "Projection du framebuffer en mémoire mmap()");

    /* 
     * Dessiner un carré de 200 pixels de côté dont le sommet supérieur gauche est
     * placé en position (100, 100) et de couleur rouge dégradée.
     */
    for ( y = 100; y < 300; y++ )
        for ( x = 100; x < 300; x++ )
        {

            location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) +
                       (y + vinfo.yoffset) * finfo.line_length;

            if ( vinfo.bits_per_pixel == 32 )
            {
		fbp[location + BLEU ] = 100;	     		/* Du bleu		*/
 		fbp[location + VERT ] = 115 + (x - 100) / 2;   	/* Un peu de vert	*/
 		fbp[location + ROUGE] = 200 - (y - 100) / 5;  	/* Beaucoup de rouge	*/
 		fbp[location + ALPHA] = 0;			/* Pas de trasparence	*/
            }
            else      /* on suppose 16 bits par pixel	*/
            {
		/*			
		 *			1    1      
		 *			5    0    5    0		
		 *			rrrrrvvvvvvbbbbb				
		 */

                int b = 10;					/* Du bleu		*/
                int g = (x - 100) / 6; 				/* Un peu de vert	*/
                int r = 31 - (y - 100) / 16;   			/* Beaucoup de rouge	*/
                unsigned short int t = r << 11 | g << 5 | b;
                *((unsigned short int*)(fbp + location)) = t;
            }

        }

    CHECK(munmap(fbp, screensize), "munmap()");
    CHECK(close(fbfd), "close()");
    return EXIT_SUCCESS;
}

